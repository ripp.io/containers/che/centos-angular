dev:
	@docker build -t=rippio/che-$(notdir $(shell pwd)):dev . 

run: dev
	@docker run -it --rm rippio/che-$(notdir $(shell pwd)):dev /bin/bash
	
prod:
	@docker build -t=rippio/che-$(notdir $(shell pwd)):latest . 

tag:
	@docker tag rippio/che-$(notdir $(shell pwd)):dev rippio/che-$(notdir $(shell pwd)):latest

push:
	@docker push rippio/che-$(notdir $(shell pwd)):latest

prod-push: prod push;