FROM eclipse/centos_jdk8

MAINTAINER Charly Rippenkroeger <docker@ripp.io>
EXPOSE 3000 4200 4300 4403 8000 8080 9000 22

LABEL che:server:3000:ref=node che:server:3000:protocol=http \
      che:server:4200:ref=angular che:server:4200:protocol=http \
      che:server:4300:ref=mocks che:server:4300:protocol=http \
      che:server:8080:ref=web  che:server:8080:protocol=http \
      che:server:9000:ref=mgmt che:server:9000:protocol=http \
      che:server:8000:ref=tomcat8-debug che:server:8000:protocol=http

USER root

# Basics
RUN yum -y update \
 && yum -y install git sudo openssh-server centos-release-scl epel-release jq vim nano \
# Node Install
 && curl --silent --location https://rpm.nodesource.com/setup_10.x | bash - \
 && yum -y install nodejs \
 && npm cache clean -f \
 && npm install -g n \
 && n stable \
 && npm install -g npm \
 && npm install -g --unsafe-perm @angular/cli \
# Cleanup
 && npm cache clean -f \
 && yum -y clean all

USER user

RUN mkdir -p /home/user/.ssh \
 && for f in "/home/user" "/etc/passwd" "/etc/group" "/projects"; do \
        sudo chgrp -R 0 ${f}  \
     && sudo chmod -R 777 ${f}; \
 done \
# SSH Needs
 && ssh-keyscan github.com >> /home/user/.ssh/known_hosts \
 && ssh-keyscan gitlab.com >> /home/user/.ssh/known_hosts

COPY ["entrypoint.sh","/home/user/entrypoint.sh"]

CMD sudo /usr/bin/ssh-keygen -A \
 && sudo /usr/sbin/sshd -D \
 && /home/user/entrypoint.sh \
 && tail -f /dev/null